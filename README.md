# [h11](https://pypi.org/project/h11/)

This is a little HTTP/1.1 library written from scratch in Python, heavily inspired by hyper-h2.

It’s a “bring-your-own-I/O” library; h11 contains no IO code whatsoever.